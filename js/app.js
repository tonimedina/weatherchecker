App = Ember.Application.create();

App.Router.map(function() {
  // put your routes here
  this.resource('forecast', {path: 'forecast/:forecast_city'});
});

App.ErrorRoute = Ember.Route.extend({
  renderTemplate: function() {
    return this.render('404');
  }
});

// Index
App.IndexRoute = Ember.Route.extend({
  activate: function() {
    Ember.$('body').addClass('home');
  },

  deactivate: function() {
    Ember.$('body').removeClass('home');
  },

  model: function() {
    return ['red', 'yellow', 'blue'];
  }
});

App.IndexController = Ember.ObjectController.extend({
  actions: {
    handleSubmit: function() {
      return this.transitionToRoute('forecast', encodeURIComponent(this.get('city').toLowerCase().replace(' ', '-')));
    }
  }
});

// Forecast
App.ForecastRoute = Ember.Route.extend({
  activate: function() {
    Ember.$('body').addClass('forecast');
  },

  deactivate: function() {
    Ember.$('body').removeClass('forecast');
  },

  model: function(params) {
    var baseURL     = 'http://query.yahooapis.com/v1/public/yql';
    var yqlQuery    = 'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="' + params.forecast_city +'")';
    var yqlQueryURL = baseURL + '?q=' + encodeURIComponent(yqlQuery) + '&format=json';

    return $.getJSON(yqlQueryURL).then(function(data) {
      // console.log(data.query.results.channel);
      
      if (data.query.results.length && data.query.results.channel.length) {
        return data.query.results.channel;
      } else {
        throw new Error('404');
      }
    });
  }
});

App.ForecastController = Ember.ObjectController.extend({
  isFahrenheit: true,
  isCelsius: false,

  actions: {
    changeUnit: function() {
      if (this.get('isFahrenheit') === true) {
        this.set('isFahrenheit', false);
        this.set('isCelsius', true);
      } else {
        this.set('isFahrenheit', true);
        this.set('isCelsius', false);
      }
    }
  }
});

// Helpers
Ember.Handlebars.helper('to-celsius', function(degrees) {
  return Math.round((degrees - 32) * 5 / 9);
});

Ember.Handlebars.helper('shorter-title', function(title) {
  return title.replace('Yahoo! Weather - ', '');
});

Ember.Handlebars.helper('format-date', function(date) {
  return moment(date).format('YYYY.MM.DD - h:mm:ss a');
});

Ember.Handlebars.helper('wi-icon', function(code) {
  switch(parseInt(code)) {
    case  0: return new Handlebars.SafeString('<i class="wi wi-tornado"></i>');
    case  1: return new Handlebars.SafeString('<i class="wi wi-storm-showers"></i>');
    case  2: return new Handlebars.SafeString('<i class="wi wi-hurricane"></i>');
    case  3: return new Handlebars.SafeString('<i class="wi wi-thunderstorm"></i>');
    case  4: return new Handlebars.SafeString('<i class="wi wi-thunderstorm"></i>');
    case  5: return new Handlebars.SafeString('<i class="wi wi-rain-mix"></i>');
    case  6: return new Handlebars.SafeString('<i class="wi wi-rain-mix"></i>');
    case  7: return new Handlebars.SafeString('<i class="wi wi-rain-mix');
    case  8: return new Handlebars.SafeString('<i class="wi wi-rain-mix"></i>');
    case  9: return new Handlebars.SafeString('<i class="wi wi-rain-mix"></i>');
    case 10: return new Handlebars.SafeString('<i class="wi wi-rain"></i>');
    case 11: return new Handlebars.SafeString('<i class="wi wi-showers"></i>');
    case 12: return new Handlebars.SafeString('<i class="wi wi-showers"></i>');
    case 13: return new Handlebars.SafeString('<i class="wi wi-snow"></i>');
    case 14: return new Handlebars.SafeString('<i class="wi wi-snow"></i>');
    case 15: return new Handlebars.SafeString('<i class="wi wi-snow"></i>');
    case 16: return new Handlebars.SafeString('<i class="wi wi-snow"></i>');
    case 17: return new Handlebars.SafeString('<i class="wi wi-hail"></i>');
    case 18: return new Handlebars.SafeString('<i class="wi wi-rain-mix"></i>');
    case 19: return new Handlebars.SafeString('<i class="wi wi-dust"></i>');
    case 20: return new Handlebars.SafeString('<i class="wi wi-fog"></i>');
    case 21: return new Handlebars.SafeString('<i class="wi wi-fog"></i>');
    case 22: return new Handlebars.SafeString('<i class="wi wi-smoke"></i>');
    case 23: return new Handlebars.SafeString('<i class="wi wi-strong-wind"></i>');
    case 24: return new Handlebars.SafeString('<i class="wi wi-windy"></i>');
    case 25: return new Handlebars.SafeString('<i class="wi wi-snowflake-cold"></i>');
    case 26: return new Handlebars.SafeString('<i class="wi wi-cloudy"></i>');
    case 27: return new Handlebars.SafeString('<i class="wi wi-night-cloudy"></i>');
    case 28: return new Handlebars.SafeString('<i class="wi wi-day-cloudy"></i>');
    case 29: return new Handlebars.SafeString('<i class="wi wi-night-cloudy"></i>');
    case 30: return new Handlebars.SafeString('<i class="wi wi-day-cloudy"></i>');
    case 31: return new Handlebars.SafeString('<i class="wi wi-night-clear"></i>');
    case 32: return new Handlebars.SafeString('<i class="wi wi-day-sunny"></i>');
    case 33: return new Handlebars.SafeString('<i class="wi wi-stars"></i>');
    case 34: return new Handlebars.SafeString('<i class="wi wi-day-sunny"></i>');
    case 35: return new Handlebars.SafeString('<i class="wi wi-hail"></i>');
    case 36: return new Handlebars.SafeString('<i class="wi wi-hot"></i>');
    case 37: return new Handlebars.SafeString('<i class="wi wi-thunderstorm"></i>');
    case 38: return new Handlebars.SafeString('<i class="wi wi-thunderstorm"></i>');
    case 39: return new Handlebars.SafeString('<i class="wi wi-thunderstorm"></i>');
    case 40: return new Handlebars.SafeString('<i class="wi wi-showers"></i>');
    case 41: return new Handlebars.SafeString('<i class="wi wi-snow"></i>');
    case 42: return new Handlebars.SafeString('<i class="wi wi-snow"></i>');
    case 43: return new Handlebars.SafeString('<i class="wi wi-snow"></i>');
    case 44: return new Handlebars.SafeString('<i class="wi wi-cloudy"></i>');
    case 45: return new Handlebars.SafeString('<i class="wi wi-storm-showers"></i>');
    case 46: return new Handlebars.SafeString('<i class="wi wi-snow"></i>');
    case 47: return new Handlebars.SafeString('<i class="wi wi-thunderstorm"></i>');
    default: return new Handlebars.SafeString('<i class="wi wi-cloud"></i>');
  }
});
