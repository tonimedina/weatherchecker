App = Ember.Application.create()
App.Router.map ->
  
  # put your routes here
  @resource "forecast",
    path: "forecast/:forecast_city"

  return


# Index
App.IndexRoute = Ember.Route.extend(
  activate: ->
    Ember.$("body").addClass "home"
    return

  deactivate: ->
    Ember.$("body").removeClass "home"
    return

  model: ->
    [
      "red"
      "yellow"
      "blue"
    ]
)
App.IndexController = Ember.ObjectController.extend(actions:
  handleSubmit: ->
    @transitionToRoute "forecast", encodeURIComponent(@get("city").toLowerCase().replace(" ", "-"))
)

# Forecast
App.ForecastRoute = Ember.Route.extend(
  activate: ->
    Ember.$("body").addClass "forecast"
    return

  deactivate: ->
    Ember.$("body").removeClass "forecast"
    return

  model: (params) ->
    baseURL = "http://query.yahooapis.com/v1/public/yql"
    yqlQuery = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"" + params.forecast_city + "\")"
    yqlQueryURL = baseURL + "?q=" + encodeURIComponent(yqlQuery) + "&format=json"
    $.getJSON(yqlQueryURL).then (data) ->
      
      # console.log(data.query.results.channel);
      data.query.results.channel

)
App.ForecastController = Ember.ObjectController.extend(
  isFahrenheit: true
  isCelsius: false
  actions:
    changeUnit: ->
      if @get("isFahrenheit") is true
        @set "isFahrenheit", false
        @set "isCelsius", true
      else
        @set "isFahrenheit", true
        @set "isCelsius", false
      return
)

# Helpers
Ember.Handlebars.helper "to-celsius", (degrees) ->
  Math.round (degrees - 32) * 5 / 9

Ember.Handlebars.helper "shorter-title", (title) ->
  title.replace "Yahoo! Weather - ", ""

Ember.Handlebars.helper "format-date", (date) ->
  moment(date).format "YYYY.MM.DD - h:mm:ss a"

Ember.Handlebars.helper "wi-icon", (code) ->
  switch parseInt(code)
    when 0
      new Handlebars.SafeString("<i class=\"wi wi-tornado\"></i>")
    when 1
      new Handlebars.SafeString("<i class=\"wi wi-storm-showers\"></i>")
    when 2
      new Handlebars.SafeString("<i class=\"wi wi-hurricane\"></i>")
    when 3
      new Handlebars.SafeString("<i class=\"wi wi-thunderstorm\"></i>")
    when 4
      new Handlebars.SafeString("<i class=\"wi wi-thunderstorm\"></i>")
    when 5
      new Handlebars.SafeString("<i class=\"wi wi-rain-mix\"></i>")
    when 6
      new Handlebars.SafeString("<i class=\"wi wi-rain-mix\"></i>")
    when 7
      new Handlebars.SafeString("<i class=\"wi wi-rain-mix")
    when 8
      new Handlebars.SafeString("<i class=\"wi wi-rain-mix\"></i>")
    when 9
      new Handlebars.SafeString("<i class=\"wi wi-rain-mix\"></i>")
    when 10
      new Handlebars.SafeString("<i class=\"wi wi-rain\"></i>")
    when 11
      new Handlebars.SafeString("<i class=\"wi wi-showers\"></i>")
    when 12
      new Handlebars.SafeString("<i class=\"wi wi-showers\"></i>")
    when 13
      new Handlebars.SafeString("<i class=\"wi wi-snow\"></i>")
    when 14
      new Handlebars.SafeString("<i class=\"wi wi-snow\"></i>")
    when 15
      new Handlebars.SafeString("<i class=\"wi wi-snow\"></i>")
    when 16
      new Handlebars.SafeString("<i class=\"wi wi-snow\"></i>")
    when 17
      new Handlebars.SafeString("<i class=\"wi wi-hail\"></i>")
    when 18
      new Handlebars.SafeString("<i class=\"wi wi-rain-mix\"></i>")
    when 19
      new Handlebars.SafeString("<i class=\"wi wi-dust\"></i>")
    when 20
      new Handlebars.SafeString("<i class=\"wi wi-fog\"></i>")
    when 21
      new Handlebars.SafeString("<i class=\"wi wi-fog\"></i>")
    when 22
      new Handlebars.SafeString("<i class=\"wi wi-smoke\"></i>")
    when 23
      new Handlebars.SafeString("<i class=\"wi wi-strong-wind\"></i>")
    when 24
      new Handlebars.SafeString("<i class=\"wi wi-windy\"></i>")
    when 25
      new Handlebars.SafeString("<i class=\"wi wi-snowflake-cold\"></i>")
    when 26
      new Handlebars.SafeString("<i class=\"wi wi-cloudy\"></i>")
    when 27
      new Handlebars.SafeString("<i class=\"wi wi-night-cloudy\"></i>")
    when 28
      new Handlebars.SafeString("<i class=\"wi wi-day-cloudy\"></i>")
    when 29
      new Handlebars.SafeString("<i class=\"wi wi-night-cloudy\"></i>")
    when 30
      new Handlebars.SafeString("<i class=\"wi wi-day-cloudy\"></i>")
    when 31
      new Handlebars.SafeString("<i class=\"wi wi-night-clear\"></i>")
    when 32
      new Handlebars.SafeString("<i class=\"wi wi-day-sunny\"></i>")
    when 33
      new Handlebars.SafeString("<i class=\"wi wi-stars\"></i>")
    when 34
      new Handlebars.SafeString("<i class=\"wi wi-day-sunny\"></i>")
    when 35
      new Handlebars.SafeString("<i class=\"wi wi-hail\"></i>")
    when 36
      new Handlebars.SafeString("<i class=\"wi wi-hot\"></i>")
    when 37
      new Handlebars.SafeString("<i class=\"wi wi-thunderstorm\"></i>")
    when 38
      new Handlebars.SafeString("<i class=\"wi wi-thunderstorm\"></i>")
    when 39
      new Handlebars.SafeString("<i class=\"wi wi-thunderstorm\"></i>")
    when 40
      new Handlebars.SafeString("<i class=\"wi wi-showers\"></i>")
    when 41
      new Handlebars.SafeString("<i class=\"wi wi-snow\"></i>")
    when 42
      new Handlebars.SafeString("<i class=\"wi wi-snow\"></i>")
    when 43
      new Handlebars.SafeString("<i class=\"wi wi-snow\"></i>")
    when 44
      new Handlebars.SafeString("<i class=\"wi wi-cloudy\"></i>")
    when 45
      new Handlebars.SafeString("<i class=\"wi wi-storm-showers\"></i>")
    when 46
      new Handlebars.SafeString("<i class=\"wi wi-snow\"></i>")
    when 47
      new Handlebars.SafeString("<i class=\"wi wi-thunderstorm\"></i>")
    else
      new Handlebars.SafeString("<i class=\"wi wi-cloud\"></i>")
